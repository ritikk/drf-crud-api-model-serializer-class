from rest_framework import serializers
from .models import Student


class StudentSerializer(
    serializers.ModelSerializer):  # ModelSerializer doesnt require to create a create and update method like we did in normal serializer. Here it happens automatically
    class Meta:
        model = Student
        fields = '__all__'

# class StudentSerializer(serializers.Serializer):
#     name = serializers.CharField(max_length=100)
#     roll = serializers.IntegerField()
#     city = serializers.CharField(max_length=50)
